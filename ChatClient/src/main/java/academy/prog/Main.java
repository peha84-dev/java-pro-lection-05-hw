package academy.prog;

import java.io.IOException;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) {
		try (Scanner scanner = new Scanner(System.in)) {
			System.out.println("Enter your login: ");
			String login = scanner.nextLine();

			Thread th = new Thread(new GetThread(login));
			th.setDaemon(true);
			th.start();

			System.out.println("Enter your message: ");
			while (true) {
				String text = scanner.nextLine();
				if (text.isEmpty()) break;

				// users
				// @test Hello

				Message m = new Message(login);

				char commandChar = text.charAt(0);
				if (commandChar == '@') {
					String[] mess = text.split(" ", 2);
					m.setTo(mess[0].substring(1));
					m.setText(mess[1]);
				} else {
					m.setText(text);
				}

				int res = m.send(Utils.getURL() + "/add");

				if (res != 200) { // 200 OK
					System.out.println("HTTP error occurred: " + res);
					return;
				}
			}
		} catch (IOException ex) {
			ex.printStackTrace();
		}
	}
}
