package academy.prog;

import java.util.Date;

public class User {
    private Date date = new Date();
    private String login;

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return login + ": last message was [" + date + "]";
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
